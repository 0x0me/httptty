# HttpTty

[HttpTty](https://httptty.mindcrime.dev/) shall be a [HTTPIE](https://httpie.org/) clone written in Rust. Being a holiday season project it focuses on exploring and practicing the Rust language.  It is neither nearly feature-complete nor ready for any kind of serious or productive use.

# License

GNU General Public License v3.0 or later

See COPYING to see the full text.
