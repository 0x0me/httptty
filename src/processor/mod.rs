pub mod json_path;

/// Common trait for various (response) body processors.
/// A body processer transforms the response body or a part
/// of it, eg. by evaluating selection expressions.
pub trait BodyProcessor {
    fn process(&self, body: String) -> String; 
}
