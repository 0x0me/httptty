use super::BodyProcessor;

use jsonpath::Selector;

/// Evalutes a JSON path expression on the response body
/// yielding the selection result.
/// ```
/// let p = JsonPathProcesser::new("$.title");
/// let result = p.process("{'title':'Evaluating JSON path'}");
/// println!("{}", result);
/// assert_eq!(result,"Evaluating JSON path");
/// ```
pub struct JsonPathProcessor {
    path: String,
}

impl JsonPathProcessor {
    pub fn new(path: String) -> JsonPathProcessor {
        JsonPathProcessor { path }
    }
}

impl BodyProcessor for JsonPathProcessor {
    fn process(&self, body: String) -> String {
        let full_body = serde_json::from_str(body.as_str()).unwrap();

        let mut selector = Selector::new();

        let result = selector
            .str_path(self.path.as_str())
            .unwrap()
            .value(&full_body)
            .select()
            .unwrap();
        if result.len() <= 1 {
            serde_json::to_string_pretty(&result.first()).unwrap()
        } else {
            serde_json::to_string_pretty(&result).unwrap()
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::processor::json_path::JsonPathProcessor;
    use crate::processor::BodyProcessor;

    #[test]
    fn test_simple_json_path() {
        let testee = JsonPathProcessor::new("$.body".to_string());
        let json: serde_json::Value = serde_json::from_str(r#"{"body":"String"}"#).unwrap();
        let result = testee.process(json.to_string());
        assert_eq!("\"String\"", result);
    }

    #[test]
    #[should_panic(expected = "expected value")]
    fn test_no_json() {
        let testee = JsonPathProcessor::new("$.body".to_string());
        let result = testee.process("adkajdajdj".to_string());
        assert_eq!("\"String\"", result);
    }
}
