use ansi_term::Colour;

use super::Formatter;

use syntect::easy::HighlightLines;
use syntect::highlighting::{Style, ThemeSet};
use syntect::parsing::SyntaxSet;
use syntect::util::{as_24_bit_terminal_escaped, LinesWithEndings};

pub struct FancyFormatter {}

impl FancyFormatter {
    pub fn new() -> FancyFormatter {
        FancyFormatter {}
    }
}

impl Formatter for FancyFormatter {
    fn print_request_str(&self, line_number: u32, method: String, url: String) -> String {
        format!(
            "{}| {} {}",
            Colour::White.dimmed().paint(format!("<{:>4}", line_number)),
            Colour::Cyan.bold().paint(method),
            Colour::Blue.underline().paint(url)
        )
    }
    fn print_http_header_request(&self, line_number: u32, key: String, value: String) -> String {
        format!(
            "{}| {}: {}",
            Colour::White.dimmed().paint(format!("<{:>4}", line_number)),
            Colour::Blue.bold().paint(key),
            Colour::Blue.paint(value)
        )
    }

    fn print_http_header_response(&self, line_number: u32, key: String, value: String) -> String {
        format!(
            "{}| {}: {}",
            Colour::White.dimmed().paint(format!(">{:>4}", line_number)),
            Colour::Blue.bold().paint(key),
            Colour::Blue.paint(value)
        )
    }

    fn print_response_str(
        &self,
        line_number: u32,
        version: reqwest::Version,
        status: reqwest::StatusCode,
    ) -> String {
        let http = match version {
            reqwest::Version::HTTP_09 => format!(
                "{}{}{}",
                Colour::Blue.paint("HTTP"),
                Colour::White.dimmed().paint("/"),
                Colour::Cyan.paint("0.9")
            ),
            reqwest::Version::HTTP_10 => format!(
                "{}{}{}",
                Colour::Blue.paint("HTTP"),
                Colour::White.dimmed().paint("/"),
                Colour::Cyan.paint("1.0")
            ),
            reqwest::Version::HTTP_11 => format!(
                "{}{}{}",
                Colour::Blue.paint("HTTP"),
                Colour::White.dimmed().paint("/"),
                Colour::Cyan.paint("1.1")
            ),
            reqwest::Version::HTTP_2 => format!(
                "{}{}{}",
                Colour::Blue.paint("HTTP"),
                Colour::White.dimmed().paint("/"),
                Colour::Cyan.paint("2")
            ),
            reqwest::Version::HTTP_3 => format!(
                "{}{}{}",
                Colour::Blue.paint("HTTP"),
                Colour::White.dimmed().paint("/"),
                Colour::Cyan.paint("3")
            ),
            _ => panic!("Unknown HTTP Version"),
        };

        format!(
            "{}| {} {} {}",
            Colour::White.dimmed().paint(format!(">{:>4}", line_number)),
            http,
            Colour::Cyan.paint(status.as_str()),
            Colour::Yellow.paint(status.canonical_reason().unwrap())
        )
    }

    /// prints the given body as colorized and pretty formatted
    // TODO: do not print direclty return a buffer or similar
    fn print_body(&self, body: String, ext: String) -> String {
        let mut result = String::new();
        let ps = SyntaxSet::load_defaults_newlines();
        let ts = ThemeSet::load_defaults();
        let syntax = ps
            .find_syntax_by_extension(&ext)
            .unwrap_or_else(|| ps.find_syntax_plain_text());
        let mut h = HighlightLines::new(syntax, &ts.themes["base16-eighties.dark"]);
        let mut i = 1;
        for line in LinesWithEndings::from(&body) {
            // LinesWithEndings enables use of newlines mode
            let ranges: Vec<(Style, &str)> = h.highlight(line, &ps);
            let escaped = as_24_bit_terminal_escaped(&ranges[..], true);
            std::fmt::write(
                &mut result,
                format_args!(
                    "{}| {}",
                    Colour::White.dimmed().paint(format!(">{:>4}", i)),
                    escaped
                ),
            )
            .unwrap();
            i += 1;
        }
        std::fmt::write(
            &mut result,
            format_args!("{}", Colour::White.reverse().paint("%")),
        )
        .unwrap();

        result
    }

    fn info_header(&self, value: String) -> String {
        format!(
            "{}",
            Colour::White.dimmed().paint(format!("----+ {}", value))
        )
    }
}
