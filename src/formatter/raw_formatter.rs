use super::Formatter;

pub struct RawFormatter {}

impl RawFormatter {
    pub fn new() -> RawFormatter {
        RawFormatter {}
    }
}

impl Formatter for RawFormatter {
    fn print_request_str(&self, _line_number: u32, method: String, url: String) -> String {
        format!("{} {}", method, url)
    }

    fn print_http_header_request(&self, _line_number: u32, key: String, value: String) -> String {
        format!("{}: {}", key, value)
    }

    fn print_http_header_response(&self, _line_number: u32, key: String, value: String) -> String {
        format!("{}: {}", key, value)
    }

    fn print_response_str(
        &self,
        _line_number: u32,
        version: reqwest::Version,
        status: reqwest::StatusCode,
    ) -> String {
        let http = match version {
            reqwest::Version::HTTP_09 => "HTTP/0.9",
            reqwest::Version::HTTP_10 => "HTTP/1.0",
            reqwest::Version::HTTP_11 => "HTTP/1.1",
            reqwest::Version::HTTP_2 => "HTTP/2",
            reqwest::Version::HTTP_3 => "HTTP/3",
            _ => panic!("Unknown HTTP Version"),
        };
        format!("{} {} {}", http, status, status.canonical_reason().unwrap())
    }

    fn print_body(&self, body: String, _ext: String) -> String {
        format!("{}%", body)
    }

    fn info_header(&self, _value: String) -> String {
        "".to_string() // noop
    }
}
