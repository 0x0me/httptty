pub mod fancy_formatter;
pub mod raw_formatter;

pub trait Formatter {
    fn print_http_header_request(&self, line_number: u32, key: String, value: String) -> String;
    fn print_http_header_response(&self, line_number: u32, key: String, value: String) -> String;

    fn print_request_str(&self, line_number: u32, method: String, url: String) -> String;

    fn print_response_str(
        &self,
        line_number: u32,
        version: reqwest::Version,
        status: reqwest::StatusCode,
    ) -> String;

    fn info_header(&self, value: String) -> String;

    fn print_body(&self, body: String, ext: String) -> String;
}
