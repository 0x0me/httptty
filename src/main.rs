extern crate ansi_term;
extern crate jsonpath_lib as jsonpath;
extern crate mime;
extern crate reqwest;
extern crate structopt;

use ansi_term::Colour;
use structopt::StructOpt;

mod formatter;
mod httptty;
mod processor;

static APP_NAME: &str = env!("CARGO_PKG_NAME");
static APP_VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Debug, Clone, StructOpt)]
#[structopt(
    name = "httptty",
    about = "A HTTP command line client",
    author = "Michael Engelhardt <me@mindcrime.dev>"
)]
pub struct Cli {
    /// the HTTP method to use
    method: String,
    /// target URL the request should be sent to
    url: String,
    /// some payload to sent with the request as body
    payload: Option<String>,
    /// print raw output
    #[structopt(short, long)]
    raw: bool,
    /// evaluate given json path on result
    #[structopt(short = "j", long = "jsonpath")]
    json_path: Option<String>,
    /// set request headers - separate multiple headers either with a withspace or repeat option
    #[structopt(short = "H", long = "header")]
    headers: Vec<String>,
    /// set user agent string
    #[structopt(long = "ua")]
    user_agent: Option<String>,
}

fn handle_error(error: &dyn std::error::Error) -> ! {
    eprintln!(
        "{} {}",
        Colour::White.dimmed().paint("----+ "),
        Colour::Red.bold().paint(format!("<==> {}", error))
    );
    std::process::exit(-1);
}

fn main() {
    #[cfg(windows)]
    let enabled = ansi_term::enable_ansi_support();
    let args = Cli::from_args();

    println!("{} {} (C) Copyright 2019, 2020 Michael Engelhardt - This program comes with ABSOLUTELY NO WARRANTY and is distributed under the terms of the GPLv3.", APP_NAME, APP_VERSION);

    let http_tty = httptty::HttpTty::new(httptty::HttpTtyOptions::from_args(args.clone()));

    let result = http_tty
        .create_request(args.method, args.url, args.payload, args.headers)
        .map(|request| {
            http_tty
                .process_request(request)
                .unwrap_or_else(|err| handle_error(&err))
        })
        .map(|request| {
            http_tty
                .execute(request)
                .unwrap_or_else(|err| handle_error(&err))
        })
        .map(|response| {
            http_tty
                .process_response(response)
                .unwrap_or_else(|err| handle_error(&err));
        });

    match result {
        Ok(_) => (),
        Err(err) => handle_error(&err),
    }
}
