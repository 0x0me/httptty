use crate::formatter::fancy_formatter::FancyFormatter;
use crate::formatter::raw_formatter::RawFormatter;
use crate::formatter::Formatter;
use crate::processor::json_path::JsonPathProcessor;
use crate::processor::BodyProcessor;
use crate::{Cli, APP_NAME, APP_VERSION};
use reqwest::header::{HeaderMap, HeaderName, HeaderValue};

pub struct HttpTtyOptions {
    user_agent: Option<String>,
    formatter: Box<dyn Formatter>,
    evaluator: Option<Box<dyn BodyProcessor>>,
}

impl HttpTtyOptions {
    /// constructor for easy test case usage
    #[cfg(test)]
    fn new(
        formatter: Box<dyn Formatter>,
        evaluator: Option<Box<dyn BodyProcessor>>,
    ) -> HttpTtyOptions {
        HttpTtyOptions {
            user_agent: None,
            formatter,
            evaluator,
        }
    }

    /// parse command line options into a httptty specific option
    /// structure
    pub fn from_args(args: Cli) -> HttpTtyOptions {
        HttpTtyOptions {
            user_agent: args.user_agent,
            formatter: if args.raw {
                Box::new(RawFormatter::new()) as Box<dyn Formatter>
            } else {
                Box::new(FancyFormatter::new()) as Box<dyn Formatter>
            },
            evaluator: if let Some(jp) = args.json_path {
                Some(Box::new(JsonPathProcessor::new(jp)) as Box<dyn BodyProcessor>)
            } else {
                None
            },
        }
    }
}

/// module for the HttpTty client
pub struct HttpTty {
    /// reference to the HTTP client
    client: reqwest::blocking::Client,
    /// formatter for output
    formatter: Box<dyn Formatter>,
    /// optional processor evaluating a certain query against the http body
    evaluator: Option<Box<dyn BodyProcessor>>,
}

/// module implementing the HttpTty client
impl HttpTty {
    pub fn new(options: HttpTtyOptions) -> HttpTty {
        let user_agent = if let Some(ua) = options.user_agent {
            ua
        } else {
            format!("{}/{}", APP_NAME, APP_VERSION)
        };

        HttpTty {
            client: reqwest::blocking::Client::builder()
                .user_agent(user_agent)
                .build()
                .unwrap(),
            evaluator: options.evaluator,
            formatter: options.formatter,
        }
    }

    /// create a new request
    /// given request method will be converted to uppercase to comply to the HTTP specification
    pub fn create_request(
        &self,
        method: String,
        url: String,
        payload: Option<String>,
        headers: Vec<String>,
    ) -> Result<reqwest::blocking::Request, reqwest::Error> {
        let m = reqwest::Method::from_bytes(method.to_uppercase().as_bytes()).unwrap();
        let builder = self.client.request(m, url.as_str());

        let header_map: HeaderMap = headers
            .into_iter()
            .map(|header| {
                let split_headers: Vec<&str> = header.splitn(2, '=').collect();
                (
                    split_headers[0].to_lowercase(),
                    split_headers[1].to_string(),
                )
            })
            .map(|(name, value)| {
                (
                    HeaderName::from_lowercase(name.as_bytes()).unwrap(),
                    HeaderValue::from_bytes(value.as_bytes()).unwrap(),
                )
            })
            .collect();

        let b = match payload {
            Some(data) => builder.body(data),
            None => builder,
        }
        .headers(header_map);
        b.build()
    }

    /// process_request processes the request and displays information aboute
    /// all interesting parameters and values
    pub fn process_request(
        &self,
        request: reqwest::blocking::Request,
    ) -> Result<reqwest::blocking::Request, reqwest::Error> {
        println!("{}", self.formatter.info_header("==> REQUEST".to_string()));
        let mut i = 1;

        println!(
            "{}",
            self.formatter.print_request_str(
                i,
                request.method().to_string(),
                request.url().as_str().to_string(),
            )
        );

        i += 1;
        for (ln, (key, value)) in request.headers().iter().enumerate() {
            println!(
                "{}",
                self.formatter.print_http_header_request(
                    i + ln as u32,
                    key.to_string(),
                    value.to_str().unwrap().to_string(),
                )
            );
        }
        Ok(request)
    }

    /// execute executes the request
    pub fn execute(
        &self,
        request: reqwest::blocking::Request,
    ) -> Result<reqwest::blocking::Response, reqwest::Error> {
        self.client.execute(request)
    }

    /// process_response prints the result of the HTTP operation
    pub fn process_response(
        &self,
        response: reqwest::blocking::Response,
    ) -> Result<(), reqwest::Error> {
        println!("{}", self.formatter.info_header("<== RESPONSE".to_string()));
        let mut i = 1;

        println!(
            "{}",
            self.formatter
                .print_response_str(i, response.version(), response.status())
        );
        i += 1;

        let response_headers = response.headers();
        {
            for (ln, (key, value)) in response_headers.iter().enumerate() {
                println!(
                    "{}",
                    self.formatter.print_http_header_response(
                        i + ln as u32,
                        key.to_string(),
                        value.to_str().unwrap().to_string(),
                    )
                );
                i += 1;
            }
        }
        let suffix = match response_headers.get("Content-Type") {
            Some(x) => {
                let s = x.to_str().unwrap();
                let m = s.parse::<mime::Mime>().unwrap();
                match m.suffix() {
                    Some(s) => s.to_string(),
                    None => m.subtype().to_string(),
                }
            }
            None => "".to_string(),
        };

        let body = response.text_with_charset("utf-8")?;
        let processed_body = if let Some(p) = &self.evaluator {
            p.process(body)
        } else {
            body
        };
        println!("{}", self.formatter.print_body(processed_body, suffix));
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::formatter::raw_formatter::RawFormatter;

    #[test]
    fn test_http_method_lowercase() {
        let testee = HttpTty::new(HttpTtyOptions::new(Box::new(RawFormatter::new()), None));
        let request = testee
            .create_request(
                "post".to_string(),
                "http://localhost".to_string(),
                None,
                Vec::new(),
            )
            .unwrap();

        let expected_method = "POST";
        assert_eq!(
            request.method().as_str(),
            expected_method,
            "{}",
            format!(
                "Unexpected method {:?} should be {:?}.",
                request.method().to_string(),
                expected_method
            )
        );
    }

    #[test]
    fn test_invalid_http_method() {
        let testee = HttpTty::new(HttpTtyOptions::new(Box::new(RawFormatter::new()), None));
        let request = testee
            .create_request(
                "POLL".to_string(),
                "http://localhost".to_string(),
                None,
                Vec::new(),
            )
            .unwrap();

        let expected_method = "POLL";
        assert_eq!(
            request.method().as_str(),
            expected_method,
            "{}",
            format!(
                "Unexpected method {:?} should be {:?}.",
                request.method().to_string(),
                expected_method
            )
        );
    }

    #[test]
    fn test_request_no_headers() {
        let testee = HttpTty::new(HttpTtyOptions::new(Box::new(RawFormatter::new()), None));

        let request = testee
            .create_request(
                "post".to_string(),
                "http://localhost".to_string(),
                None,
                Vec::new(),
            )
            .unwrap();
        assert_eq!(
            request.headers().is_empty(),
            true,
            "There should be no headers"
        );
    }

    #[test]
    fn test_request_some_headers() {
        let testee = HttpTty::new(HttpTtyOptions::new(Box::new(RawFormatter::new()), None));

        let request = testee
            .create_request(
                "post".to_string(),
                "http://localhost".to_string(),
                None,
                vec![
                    String::from("X-MY-HEADER=this is a header"),
                    String::from("X-MY-HEADER2=this=is=a==header"),
                ],
            )
            .unwrap();
        assert_eq!(
            request.headers().is_empty(),
            false,
            "There should be no headers"
        );

        let mut expected_headers = HeaderMap::new();
        expected_headers.append("X-MY-HEADER", HeaderValue::from_static("this is a header"));
        expected_headers.append(
            "X-MY-HEADER2",
            HeaderValue::from_static("this=is=a==header"),
        );

        let headers = request.headers();

        for name in headers.keys() {
            assert_eq!(
                headers.get(name).unwrap(),
                expected_headers.remove(name).unwrap()
            );
        }

        assert_eq!(
            expected_headers.len(),
            0,
            "Expected all headers to be processed!"
        );
    }
}
